package bp.gpm;

import bp.en.EntitiesMyPK;
import bp.en.Entity;

import java.util.ArrayList;
import java.util.List;

public class PowerCenters extends EntitiesMyPK {
    /**
     岗位
     */
    public PowerCenters()
    {
    }
    /**
     得到它的 Entity
     */
    @Override
    public Entity getGetNewEntity()
    {
        return new bp.gpm.PowerCenter();
    }


    ///为了适应自动翻译成java的需要,把实体转换成List.
    /**
     转化成 java list,C#不能调用.

     @return List
     */
    public final List<PowerCenter> ToJavaList()
    {
        return (List<PowerCenter>)(Object)this;
    }
    /**
     转化成list

     @return List
     */
    public final ArrayList<PowerCenter> Tolist()
    {
        ArrayList<PowerCenter> list = new ArrayList<PowerCenter>();
        for (int i = 0; i < this.size(); i++)
        {
            list.add((PowerCenter)this.get(i));
        }
        return list;
    }

    /// 为了适应自动翻译成java的需要,把实体转换成List.
}
