package bp.sys.frmui;

import bp.da.*;
import bp.en.*;
import bp.sys.*;

import bp.sys.*;
import java.util.*;

/** 
 实体属性s
*/
public class MapAttrCards extends EntitiesMyPK
{

		///构造
	/** 
	 实体属性s
	*/
	public MapAttrCards()
	{
	}
	/** 
	 得到它的 Entity
	*/
	@Override
	public Entity getGetNewEntity()
	{
		return new MapAttrCard();
	}

		///


		///为了适应自动翻译成java的需要,把实体转换成List.
	/** 
	 转化成 java list,C#不能调用.
	 
	 @return List
	*/
	public final java.util.List<MapAttrCard> ToJavaList()
	{
		return (java.util.List<MapAttrCard>)(Object)this;
	}
	/** 
	 转化成list
	 
	 @return List
	*/
	public final ArrayList<MapAttrCard> Tolist()
	{
		ArrayList<MapAttrCard> list = new ArrayList<MapAttrCard>();
		for (int i = 0; i < this.size(); i++)
		{
			list.add((MapAttrCard)this.get(i));
		}
		return list;
	}

		/// 为了适应自动翻译成java的需要,把实体转换成List.
}