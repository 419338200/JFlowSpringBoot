package bp.ccbill.template;

import bp.en.EntityNoNameAttr;

public class GroupMethodAttr extends EntityNoNameAttr {
    /// <summary>
    /// 表单ID
    /// </summary>
    public static final String FrmID = "FrmID";
    /// <summary>
    /// 顺序
    /// </summary>
    public static final String Idx = "Idx";
    /// <summary>
    /// 方法类型
    /// </summary>
    public static final String MethodType = "MethodType";
    /// <summary>
    /// 方法ID
    /// </summary>
    public static final String MethodID = "MethodID";
    /// <summary>
    /// Icon
    /// </summary>
    public static final String Icon = "Icon";
}
