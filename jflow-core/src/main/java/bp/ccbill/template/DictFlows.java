package bp.ccbill.template;

import bp.en.EntitiesMyPK;
import bp.en.Entity;

import java.util.ArrayList;
import java.util.List;

public class DictFlows extends EntitiesMyPK {
    private static final long serialVersionUID = 1L;
    ///构造方法.
    /**
     控制模型集合
     */
    public DictFlows()
    {
    }
    @Override
    public Entity getGetNewEntity()
    {
        return new DictFlow();
    }

    /// 构造方法.


    ///为了适应自动翻译成java的需要,把实体转换成List.
    /**
     转化成 java list,C#不能调用.

     @return List
     */
    public final List<DictFlow> ToJavaList()
    {
        return (List<DictFlow>)(Object)this;
    }
    /**
     转化成list

     @return List
     */
    public final ArrayList<DictFlow> Tolist()
    {
        ArrayList<DictFlow> list = new ArrayList<DictFlow>();
        for (int i = 0; i < this.size(); i++)
        {
            list.add((DictFlow)this.get(i));
        }
        return list;
    }

    /// 为了适应自动翻译成java的需要,把实体转换成List.
}
