package bp.ccbill;

import bp.da.DataType;
import bp.en.*;
import bp.sys.EnCfgAttr;
import bp.sys.FrmType;
import bp.sys.MapAttr;
import bp.sys.MapDataAttr;
import bp.web.WebUser;

public class DBList extends EntityNoName {
    ///权限控制.
    @Override
    public UAC getHisUAC() throws Exception
    {
        UAC uac = new UAC();
        if (WebUser.getNo().equals("admin") == true)
        {
            uac.IsDelete = false;
            uac.IsUpdate = true;
            return uac;
        }
        uac.Readonly();
        return uac;
    }

    public final String getPTable() throws Exception
    {
        String s=this.GetValStrByKey(MapDataAttr.PTable);
        if(DataType.IsNullOrEmpty(s))
            return this.getNo();
        return s;
    }
    public final void setPTable(String value) throws Exception
    {
        this.SetValByKey(MapDataAttr.PTable, value);
    }

    public final EntityType getEntityType() throws Exception
    {
        return EntityType.forValue(this.GetValIntByKey(DBListAttr.EntityType));
    }
    public final void setEntityType(EntityType value) throws Exception
    {
        this.SetValByKey(DBListAttr.EntityType, value.getValue());
    }
    /**
     表单类型 (0=傻瓜，2=自由 ...)
     * @throws Exception
     */
    public final FrmType getFrmType() throws Exception
    {
        return FrmType.forValue(  this.GetValIntByKey(MapDataAttr.FrmType) );
    }
    public final void setFrmType(FrmType value) throws Exception
    {
        this.SetValByKey(MapDataAttr.FrmType, value.getValue());
    }
    /**
     表单树
     * @throws Exception
     */
    public final String getFK_FormTree() throws Exception
    {
        return this.GetValStrByKey(MapDataAttr.FK_FormTree);
    }
    public final void setFK_FormTree(String value) throws Exception
    {
        this.SetValByKey(MapDataAttr.FK_FormTree, value);
    }
    /*
    新建模式 @0=表格模式@1=卡片模式@2=不可用
     */
    public final Integer getBtnNewModel() throws Exception{
        return this.GetValIntByKey(DBListAttr.BtnNewModel);
    }
    public final void setBtnNewModel(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnNewModel, value);
    }
    /**
     单据格式
     * @throws Exception
     */
    public final String getBillNoFormat() throws Exception
    {
        String str = this.GetValStrByKey(DBListAttr.BillNoFormat);
        if (DataType.IsNullOrEmpty(str) == true)
        {
            str = "{LSH4}";
        }
        return str;
    }
    public final void setBillNoFormat(String value) throws Exception
    {
        this.SetValByKey(DBListAttr.BillNoFormat, value);
    }
    /**
     单据编号生成规则
     * @throws Exception
     */
    public final String getTitleRole() throws Exception
    {
        String str = this.GetValStrByKey(DBListAttr.TitleRole);
        if (DataType.IsNullOrEmpty(str) == true)
        {
            str = "@WebUser.FK_DeptName @WebUser.Name @RDT";
        }
        return str;
    }
    public final void setTitleRole(String value) throws Exception
    {
        this.SetValByKey(DBListAttr.BillNoFormat, value);
    }
    /*
    新建标签
     */
    public final String getBtnNewLable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnNewLable);
    }
    public final void setBtnNewLable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnNewLable, value);
    }
    /*
   删除标签
    */
    public final String getBtnDelLable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnDelLable);
    }
    public final void setBtnDelLable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnDelLable, value);
    }
    /*
   保存标签
    */
    public final String getBtnSaveLable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnSaveLable);
    }
    public final void setBtnSaveLable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnSaveLable, value);
    }
    /*
  提交标签
   */
    public final String getBtnSubmitLable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnSubmitLable);
    }
    public final void setBtnSubmitLable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnSubmitLable, value);
    }
    /*
 查询标签
  */
    public final String getBtnSearchLabel() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnSearchLabel);
    }
    public final void setBtnSearchLabel(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnSearchLabel, value);
    }
    /*
数据快照
*/
    public final String getBtnDataVer() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnDataVer);
    }
    public final void setBtnDataVer(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnDataVer, value);
    }
    /*
    分组按钮
    */
    public final String getBtnGroupEnable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnGroupEnable);
    }
    public final void setBtnGroupEnable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnGroupEnable, value);
    }
    public final String getBtnGroupLabel() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnGroupLabel);
    }
    public final void setBtnGroupLabel(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnGroupLabel, value);
    }
    /*
   打印HTML按钮
   */
    public final String getBtnPrintHtmlEnable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintHtmlEnable);
    }
    public final void setBtnBtnPrintHtmlEnable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintHtmlEnable, value);
    }
    public final String getBtnPrintHtml() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintHtml);
    }
    public final void setBtnPrintHtml(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintHtml, value);
    }
    /*
   打印PDF按钮
   */
    public final String getBtnPrintPDFEnable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintPDFEnable);
    }
    public final void setBtnPrintPDFEnable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintPDFEnable, value);
    }
    public final String getBtnPrintPDF() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintPDF);
    }
    public final void setBtnPrintPDF(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintPDF, value);
    }
    /*
  打印RTF按钮
  */
    public final String getBtnPrintRTFEnable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintRTFEnable);
    }
    public final void setBtnPrintRTFEnable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintRTFEnable, value);
    }
    public final String getBtnPrintRTF() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintRTF);
    }
    public final void setBtnPrintRTF(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintRTF, value);
    }
    /*
    打印RTF按钮
    */
    public final String getBtnPrintCCWordEnable() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintCCWordEnable);
    }
    public final void setBtnPrintCCWordEnable(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintCCWordEnable, value);
    }
    public final String getBtnPrintCCWord() throws Exception{
        return this.GetValStrByKey(DBListAttr.BtnPrintCCWord);
    }
    public final void setBtnPrintCCWord(String value) throws Exception{
        this.SetValByKey(DBListAttr.BtnPrintCCWord, value);
    }
    /*
    数据源类型
    */
    public final int getDBType() throws Exception{
        return this.GetValIntByKey(MapDataAttr.DBType);
    }
    public final void setDBType(String value) throws Exception{
        this.SetValByKey(MapDataAttr.DBType, value);
    }
    public final String getDBSrc() throws Exception{
        return this.GetValStrByKey(MapDataAttr.DBSrc);
    }
    public final void setDBSrc(String value) throws Exception{
        this.SetValByKey(MapDataAttr.DBSrc, value);
    }
    public final int getExpEn() throws Exception{
        return this.GetValIntByKey(MapDataAttr.ExpEn);
    }
    public final void setExpEn(String value) throws Exception{
        this.SetValByKey(MapDataAttr.ExpEn, value);
    }
    public final String getExpList() throws Exception{
        return this.GetValStrByKey(MapDataAttr.ExpList);
    }
    public final void setExpList(String value) throws Exception{
        this.SetValByKey(MapDataAttr.ExpList, value);
    }
    public final int getExpCount() throws Exception{
        return this.GetValIntByKey(MapDataAttr.ExpCount);
    }
    public final void setExpCount(String value) throws Exception{
        this.SetValByKey(MapDataAttr.ExpCount, value);
    }
    public final int getMainTable() throws Exception{
        return this.GetValIntByKey(DBListAttr.MainTable);
    }
    public final void setMainTable(String value) throws Exception{
        this.SetValByKey(DBListAttr.MainTable, value);
    }
    public final int getMainTablePK() throws Exception{
        return this.GetValIntByKey(DBListAttr.MainTablePK);
    }
    public final void setMainTablePK(String value) throws Exception{
        this.SetValByKey(DBListAttr.MainTablePK, value);
    }

    public DBList(){

    }

    public DBList(String no) throws Exception{
        this.setNo(no);
        this.Retrieve();
    }

    /**
     EnMap
     */
    @Override
    public Map getEnMap() throws Exception
    {
        if (this.get_enMap() != null)
        {
            return this.get_enMap();
        }
        Map map = new Map("Sys_MapData", "数据源实体");
        map.setCodeStruct("4");

        map.AddTBStringPK(MapDataAttr.No, null, "表单编号", true, true, 1, 190, 20);
        map.SetHelperAlert(MapDataAttr.No, "也叫表单ID,系统唯一.");

        map.AddDDLSysEnum(MapDataAttr.FrmType, 0, "表单类型", true, true, "BillFrmType", "@0=傻瓜表单@1=自由表单@8=开发者表单");
        map.AddTBString(MapDataAttr.Name, null, "表单名称", true, false, 0, 200, 20, true);

        map.AddTBInt(MapDataAttr.DBType, 0, "数据源类型", true, true);
        map.AddTBString(MapDataAttr.DBSrc, null, "数据源", false, false, 0, 600, 20);

        map.AddTBString(MapDataAttr.ExpEn, null, "实体数据源", false, false, 0, 600, 20, true);
        map.AddTBString(MapDataAttr.ExpList, null, "列表数据源", false, false, 0, 600, 20, true);

        map.AddTBString(DBListAttr.MainTable, null, "列表数据源主表", false, false, 0, 50, 20, false);
        map.AddTBString(DBListAttr.MainTablePK, null, "列表数据源主表主键", false, false, 0, 50, 20, false);
        map.AddTBString(MapDataAttr.ExpCount, null, "列表总数", false, false, 0, 600, 20, true);

        map.AddDDLSysEnum(FrmAttr.RowOpenModel, 2, "行记录打开模式", true, true,
                "RowOpenMode", "@0=新窗口打开@1=在本窗口打开@2=弹出窗口打开,关闭后不刷新列表@3=弹出窗口打开,关闭后刷新列表");
        String cfg = "@0=MyDictFrameWork.htm 实体与实体相关功能编辑器";
        cfg += "@1=MyDict.htm 实体编辑器";
        cfg += "@2=MyBill.htm 单据编辑器";
        cfg += "@9=自定义URL";
        map.AddDDLSysEnum("SearchDictOpenType", 0, "双击行打开内容", true, true, "SearchDictOpenType", cfg);
        map.AddTBString(EnCfgAttr.UrlExt, null, "要打开的Url", true, false, 0, 500, 60, true);
        map.AddTBInt(FrmAttr.PopHeight, 500, "弹窗高度", true, false);
        map.AddTBInt(FrmAttr.PopWidth, 760, "弹窗宽度", true, false);

        map.AddDDLSysEnum(MapDataAttr.TableCol, 0, "表单显示列数", true, true, "傻瓜表单显示方式",
                "@0=4列@1=6列@2=上下模式3列");

        map.AddDDLSysEnum(FrmAttr.EntityEditModel, 0, "编辑模式", true, true, FrmAttr.EntityEditModel, "@0=表格@1=行编辑");
        map.SetHelperAlert(FrmAttr.EntityEditModel, "用什么方式打开实体列表进行编辑0=只读查询模式SearchDict.htm,1=行编辑模式SearchEditer.htm");

        map.AddDDLSysEnum(DBListAttr.EntityType, 0, "业务类型", true, false, DBListAttr.EntityType,
                "@0=独立表单@1=单据@2=编号名称实体@3=树结构实体");
        map.SetHelperAlert(DBListAttr.EntityType, "该实体的类型,@0=单据@1=编号名称实体@2=树结构实体.");

        map.AddTBString(DBListAttr.BillNoFormat, null, "实体编号规则", true, false, 0, 100, 20, true);
        map.SetHelperAlert(DBListAttr.BillNoFormat, "\t\n实体编号规则: \t\n 2标识:01,02,03等, 3标识:001,002,003,等..");
        map.AddTBString(FrmBillAttr.SortColumns, null, "排序字段", true, false, 0, 100, 20, true);
        map.AddTBString(FrmBillAttr.ColorSet, null, "颜色设置", true, false, 0, 100, 20, true);
        map.AddTBString(FrmBillAttr.FieldSet, null, "字段求和求平均设置", true, false, 0, 100, 20, true);

        //字段格式化函数.
        map.AddTBString("ForamtFunc", null, "字段格式化函数", true, false, 0, 200, 60, true);
        String msg = "对字段的显示使用函数进行处理";
        msg += "\t\n 1. 对于字段内容需要处理后在输出出来.";
        msg += "\t\n 2. 比如：原字段内容 @zhangsa,张三@lisi,李四 显示的内容为 张三,李四";
        msg += "\t\n 3. 配置格式: 字段名@函数名; 比如:  FlowEmps@DealFlowEmps; ";
        msg += "\t\n 4. 函数写入到 \\DataUser\\JSLibData\\SearchSelf.js";
        map.SetHelperAlert("ForamtFunc", msg);
        //增加参数字段.
        map.AddTBAtParas(4000);

        RefMethod rm = new RefMethod();

        rm = new RefMethod();
        rm.Title = "步骤1: 设置数据源."; // "设计表单";
        rm.ClassMethodName = this.toString() + ".DoDBSrc";
        rm.Icon = "../../WF/Img/Event.png";
        rm.Visable = true;
        rm.refMethodType = RefMethodType.RightFrameOpen;
        rm.Target = "_blank";
        map.AddRefMethod(rm);


        rm = new RefMethod();
        rm.Title = "步骤2: 实体数据"; // "设计表单";
        rm.ClassMethodName = this.toString() + ".DoExpEn";
        rm.Icon = "../../WF/Img/Event.png";
        rm.Visable = true;
        rm.refMethodType = RefMethodType.RightFrameOpen;
        rm.Target = "_blank";
        map.AddRefMethod(rm);

        rm = new RefMethod();
        rm.Title = "步骤3: 列表数据"; // "设计表单";
        rm.ClassMethodName = this.toString() + ".DoExpList";
        rm.Icon = "../../WF/Img/Event.png";
        rm.Visable = true;
        rm.refMethodType = RefMethodType.RightFrameOpen;
        rm.Target = "_blank";
        map.AddRefMethod(rm);

        rm = new RefMethod();
        rm.Title = "步骤4: 测试"; // "设计表单";
        rm.ClassMethodName = this.toString() + ".DoDBList";
        rm.Icon = "../../WF/Img/Event.png";
        rm.Visable = true;
        rm.refMethodType = RefMethodType.RightFrameOpen;
        rm.Target = "_blank";
        map.AddRefMethod(rm);


        rm = new RefMethod();
        rm.Title = "查询条件"; // "设计表单";
        //   rm.GroupName = "高级选项";
        rm.ClassMethodName = this.toString() + ".DoSearch";
        rm.Icon = "../../WF/Img/Event.png";
        rm.Visable = true;
        rm.refMethodType = RefMethodType.RightFrameOpen;
        rm.Target = "_blank";
        map.AddRefMethod(rm);

        rm = new RefMethod();
        rm.Title = "视频教程"; // "设计表单";
        rm.ClassMethodName = this.toString() + ".DoVideo";
        rm.Visable = true;
        rm.refMethodType = RefMethodType.LinkeWinOpen;
        rm.Target = "_blank";
        map.AddRefMethod(rm);

        this.set_enMap(map);
        return this.get_enMap();
    }
}
