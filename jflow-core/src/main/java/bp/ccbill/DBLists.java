package bp.ccbill;

import bp.en.EntitiesNoName;
import bp.en.Entity;

import java.util.ArrayList;
import java.util.List;

public class DBLists extends EntitiesNoName {
    ///构造
    /**
     单据属性s
     */
    public DBLists()
    {
    }
    /**
     得到它的 Entity
     */
    @Override
    public Entity getGetNewEntity()
    {
        return new DBList();
    }

    ///


    ///为了适应自动翻译成java的需要,把实体转换成List.
    /**
     转化成 java list,C#不能调用.

     @return List
     */
    public final List<DBList> ToJavaList()
    {
        return (List<DBList>)(Object)this;
    }
    /**
     转化成list

     @return List
     */
    public final ArrayList<DBList> Tolist()
    {
        ArrayList<DBList> list = new ArrayList<DBList>();
        for (int i = 0; i < this.size(); i++)
        {
            list.add((DBList)this.get(i));
        }
        return list;
    }

    /// 为了适应自动翻译成java的需要,把实体转换成List.
}
